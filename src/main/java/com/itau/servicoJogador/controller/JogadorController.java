package com.itau.servicoJogador.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.servicoJogador.models.Jogador;
import com.itau.servicoJogador.repositories.JogadorRepository;

@RestController
@RequestMapping("/jogador")
public class JogadorController {


	@Autowired
	JogadorRepository jogadorRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<Jogador> buscar() {
		return jogadorRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Jogador criar(@Valid @RequestBody Jogador jogador) {
		return jogadorRepository.save(jogador);
	}	
	
	@RequestMapping(path="/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> buscarJogador (@PathVariable long id) {
	
		Optional<Jogador> jogadorOptional = jogadorRepository.findById(id);
		
		if(!jogadorOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(jogadorOptional.get());
	}
}
