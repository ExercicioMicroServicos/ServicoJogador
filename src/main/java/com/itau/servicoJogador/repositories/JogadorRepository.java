package com.itau.servicoJogador.repositories;

import org.springframework.data.repository.CrudRepository;
import com.itau.servicoJogador.models.Jogador;

public interface JogadorRepository extends CrudRepository <Jogador, Long> {

}
